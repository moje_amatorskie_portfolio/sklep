<?php
 session_start();
 setcookie(session_name(),session_id(),time()+600000,'/');
 //if(isset($_COOKIE['PHPSESSID']))print $_COOKIE['PHPSESSID'];
 use PHPMailer\PHPMailer\PHPMailer;         //this must be at the top of php file
 use PHPMailer\PHPMailer\Exception;
 require 'vendor/autoload.php';
 require 'dane.php';
?>

<!DOCTYPE html>
<html lang="pl">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Sklep wirtualny</title>
  <meta name="description" content="Internetowy sklep wielobranżowy ">
  <meta name="keywords" content="sklep online, zakupy internetowe" >
  <link rel="stylesheet" href="css_projekt.css">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
 <header>
  <nav class='navbar navbar-dark bg-primary navbar-expand-lg'>
    <a class='navbar-brand' href='./koncowy.php'>Sklep</a>
    <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#mainmenu'>
      <span class='navbar-toggler-icon'></span>
    </button>
    <div class='collapse navbar-collapse' id='mainmenu'>
      <ul class='navbar-nav mr-auto'>
        <!--<li class='nav-item dropdown'>
          <a class='nav-link dropdown-toggle' href='koncowy.php' data-toggle='dropdown'  role='button' id='submenu' >Kategorie</a>
          <div class='dropdown-menu' >
            <span class='dropdown-item'  id='kwiaty' onclick='funkcja(1)'>Kwiaty</span>
            <a class='dropdown-item' href='#' id='iglaki' onclick='funkcja(2)'>Drzewa Iglaste</a>
            <a class='dropdown-item' href='#' id='lisciaste' onclick='funkcja(3)'>Drzewa Liściaste</a>
            <a class='dropdown-item' href='#' id='roze' onclick='funkcja(4)'>Róże</a>
            <a class='dropdown-item' href='#' id='preparaty' onclick='funkcja(5)'>Preparaty</a>
          </div>
        </li>-->
        <li class='nav-item' hidden>
          <a class='nav-link' hidden id='historia' href='#'>Historia</a>
        </li>
<?php

$paymentSuccessful = false;
if(!empty($_GET['message'])) {
  $paymentSuccessful = true;
}

if(!isset($_SERVER['REQUEST_METHOD']) || $_SERVER['REQUEST_METHOD']!='POST') {
    $_POST['cena_min']='0';
    $_SERVER['REQUEST_METHOD']='POST';
}

if(!isset($_SESSION['login'])){
  print "
    <li class='nav-item'>
      <a class='nav-link' href='./rejestracja.php'>Rejestracja</a>
    </li>
    <li class='nav-item'>
      <a class='nav-link' href='javascript:void(0)'  onclick='drugi_formularz_pokaz_koszyk($paymentSuccessful)'>Koszyk</a>
    </li>
<li class='nav-item'>
      <a class='nav-link' href='../portfolio/public'  >POWRÓT DO PROJEKTÓW</a>
    </li>

  ";
}else if($_SESSION['login']!='admin'){
  print "
    <li class='nav-item'>
      <a class='nav-link' href='./wylogowanie.php'>Wyloguj</a>
    </li>
    <li class='nav-item'>
      <a class='nav-link' href='javascript:void(0)'  onclick='drugi_formularz_pokaz_koszyk($paymentSuccessful)'>Koszyk</a>
    </li>
<li class='nav-item'>
      <a class='nav-link' href='../portfolio/public'  >POWRÓT DO PROJEKTÓW</a>
    </li>

  ";
}else if($_SESSION['login']=='admin'){
  print "
    <li class='nav-item'>
      <a class='nav-link' href='./wylogowanie.php'>Wyloguj</a>
    </li>
<li class='nav-item'>
      <a class='nav-link' href='../portfolio/public'  >POWRÓT DO PROJEKTÓW</a>
    </li>

  ";
}
if(isset($_SESSION['zalogowany'])&&($_SESSION['zalogowany']=='yes') && ($_SESSION['login']!='admin') ){
  //print"<li class='nav-item'><a class='nav-link' href='#'>$_SESSION[imie]</a></li>";
  print "
    <li class='nav-item'>
      <a class='nav-link' href='#' onclick='#'>$_SESSION[imie]</a>
    <li class='nav-item'>
      <a class='nav-link' href='#' onclick='form_moje_zamowienia()'>Moje zamówienia</a>
    </li>        ";
}else if( (isset ($_SESSION['login']) ) && ($_SESSION['login']=='admin') ){
  print "
    <li>
        <a class='nav-link' href='#' onclick=''>Admin</a>
    </li>
     <li class='nav-item lapka'>
      <a class='nav-link lapka' href='./dodaj.php'>Dodaj Przedmiot</a>
    </li>
     <li class='nav-item lapka'>
      <a class='nav-link' href='#' onclick='form_moje_zamowienia()'>Zamówienia</a>
    </li>
  ";
}else
  {
    print "
        <li class='nav-item'>
          <a id='logowanie_menu' class='nav-link' href='./logowanie.php'>Logowanie</a>
        </li>";
  }
?>
        <li class='nav-item'>
          <a class='nav-link' href='./kontakt.php'>Kontakt</a>
        </li>
      </ul>
      <form class='form-inline' action='#' method='POST'>
        <input class='form-control mr-1' type='search' placeholder='Wyszukaj' name='wyszukaj'>
        <button class='btn btn-light' type='submit'>Znajdź</button>
      </form>
    </div>
  </nav>
</header>
<main>
<!-- ________________________________________ boczny pasek ________________________________________________ -->
<div id='nav' class='d-none d-xs-none d-sm-block' style='height:100vh'>
  <nav class='navbar navbar-expand-sm'>
    <div class='collapse navbar-collapse' id='boczne_menu'>
      <form action='#' method='post' >
        Cena<br>
        <input class='dopasuj' type='number' name='cena_min' placeholder='od' value='0'>-
        <input class='dopasuj' type='number' name='cena_max' placeholder='do'><br>
        <button  type='submit'>Filtruj</button>
      </form>
    </div>
  </nav>
</div>
<!--  ___________________________________________koniec boczny pasek_________________________________ -->
<?php

//if(isset($_SERVER['REQUEST_METHOD']))print "metoda rekłesta:".$_SERVER['REQUEST_METHOD'];
//if(isset($_POST['pokaz_id']))print "pokaz_id:$_POST[pokaz_id]";
?>
<section class='jumpers '>
<?php
if(isset($_POST['ukryty']))print "<h2><center>".$_POST['ukryty']."</h2></center>"; //pokazanie  tytulu kategori
?>
<?php
/*if(!isset($_SESSION['login']) && !$_SESSION['login']){
  print "";
}*/
if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD']=='POST' ){  //...............poczatek duzego if POST.................
try{  //----------------------------------------------------------------------------------------poczatek duzego try
//....................................................Dodanie do koszyka ................................................

if(!empty($_POST['id_do_koszyka'])){

  dodajDoKoszyka($_POST['id_do_koszyka'],$_POST['nazwa_do_koszyka'],$_POST['liczba_do_koszyka'],$_POST['cena_do_koszyka']);

}
//....................................................Koniec Dodania do koszyka..........................................

//----------------------------------------------------Pokazanie koszyka   --------------------------------
if(!empty($_POST['pokaz_koszyk']))
  pokazKoszyk($_POST['pokaz_cena'],$_POST['usun_z_koszyka']);
//-------------------------------------------------   Koniec Pokazania koszyka--------------------------------

//----------------------------------------------------Złożenie zamówienia---------------------------------------
if(!empty($_POST['zamowienie']))zloz_zamowienie();
//----------------------------------------------------Koniec złożenia zamówienia-----------------------------------------

if(!empty($_GET['message'])) {
  showMessage();
}

//
//=====================================================Pokazanie zamowien=====================================
if($_POST['form_pokaz_zamowienia']) form_pokaz_zamowienia($_POST['zamowienie_do_usuniecia']);
//=====================================================Koniec pokazania zamowienia=============================
//
//-----------------------------------------------------Pokazanie pojedynczego zamowienia------------------------
if($_POST['numer_zamowienia']){
  pojedynczeZamowienie($_POST['numer_zamowienia']);
}
//-----------------------------------------------------Koniec pokazania pojedynczego zamowienia----------------------------
//
//-----------------------------------------------------Usunięcie przedmiotu------------------------------------------------
//if(empty($_POST['temporacyjny']) )print "empty post temporacyjny true"; //empty() jest najlepsza do tego,lepsza niz isset
//if($_POST[pokaz_id])print "pokaz_id:$_POST[pokaz_id]";
$db=new PDO('mysql:host=localhost;dbname=db','uzytkownik','pass');
$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
$db->query("SET CHARSET utf8");    //bez tego nie będzie polskich czcionek przesłanych z php
$db->query("SET NAMES 'utf8' COLLATE 'utf8_polish_ci'"); ////bez tego nie będzie polskich czcionek przesłanych z php
if(($_POST['usun_id'])){
  $db->exec("delete from przedmioty where id='$_POST[usun_id]'");
  print "<h2><center>Przedmiot został usunięty</center></h2>";
  //if(empty($_POST['pokaz_id']))print"empty post pokaz id=true<br>";  //to dobry sposob
  //if(isset($_POST['pokaz_id']))print"isset post pokaz id=true<br>";// pokazuje jak useless jest isset
  //if($_POST['pokaz_id'])print "if post pokaz id =true<br>";   // nawet ten lepszy od isset
}
//----------------------------------------------------Koniec usuwania przedmiotu------------------------------------------
// if form is submitted then input:
// pusty===false is false, 0===false is false, 5===false is false,
// pusty== false is true,  0== false is true , 5== false is false,
// pusty===true  is false, 0===true  is false, 5===true  is false,
// pusty== true  is false, 0== true  is false, 5== true  is true,
// $_POST['cena_min']==0 jest true gdy input jest empty czyli musi byc ===
//------------------------------------------------------Filtrowanie  według ceny--------------------------------------
if(isset($_POST['ukryty']))
  $q=$db->query("select nazwa,zdjecie,cena,id from przedmioty where kategoria='$_POST[ukryty]'");
//isset jest true jesli nawet pusty input wyslemy w tej samej formie,
if((($_POST['cena_min']==true)|| $_POST['cena_min']==='0') && ($_POST['cena_max']==false) && ($_POST['cena_max']!=='0') )
  $q=$db->query("select nazwa,zdjecie,cena,id from przedmioty where cena>$_POST[cena_min] order by cena");
if(($_POST['cena_max']==true)&& ($_POST['cena_min']==false) && ($_POST['cena_min']!=='0') )
  $q=$db->query("select nazwa,zdjecie,cena,id from przedmioty where cena<$_POST[cena_max] order by cena");
if(($_POST['cena_min']==true || $_POST['cena_min']==='0') && ($_POST['cena_max']==true) && ($_POST['cena_max']!=='0'))
  $q=$db->query("select nazwa,zdjecie,cena,id from przedmioty where cena>$_POST[cena_min] and cena<$_POST[cena_max] order by cena");
//------------------------------------------------------ Koniec filrowania wg ceny-----------------------------------

//-------------------------------------------------------Wyszukiwanie wg wpisanego słowa-------------------------------
if($_POST['wyszukaj']){
  $q=$db->query("select nazwa,zdjecie,cena,id from przedmioty where nazwa like '%$_POST[wyszukaj]%' order by cena ");
}
//-------------------------------------------------------Zmiana danych przedmiotu-------------------------------------

if( ($_POST['zmieniony']) ){
  //print "<br>  $_POST[zmien_nazwa].$_POST[zmien_opis].$_POST[zmien_cena].$_POST[zmieniony_id]<br>";
  $stmt=$db->prepare("update przedmioty set nazwa=?,cena=?,opis=? where id='$_POST[zmieniony_id]'");        //przy wstawiasniu ? nie trzeba dawac single quotes like '?' bo funkcja execute sama to robi
  $stmt->execute(array($_POST['zmien_nazwa'],$_POST['zmien_cena'],$_POST['zmien_opis']));
  //$db->exec("update przedmioty set nazwa='$_POST[zmien_nazwa]',cena='$_POST[zmien_cena]',opis='$_POST[zmien_opis]'  where id='$_POST[zmieniony_id]'");
  print "<h1 style='text-align:center'>Zmiany zostały zapisane</h1>";
}

//--------------------------------------------------------------------------------------------------------------------------
if(isset($q))$tab=$q->fetchall(PDO::FETCH_ASSOC); //jak by tu nie bylo isset to sie zawiesza , dalej nie idzie, i nie pokazuje błędu w ogóle - pół dnia szukałem problemu!!!
print "


  <div class='container '>

<div class='  ' style=''> <!-- div nr 46 -->
      <h2>
     <!--   <center><br><br>Wirtualny Sklep Ogrodniczy<center>
      </h2>
      <h5>

        Ta strona to mój  projekt symulacji sklepu internetowego.
         Jest to strona responsywna i generowana dynamicznie na VPS Apache w PHP,JavaScript i Mysql(bez żadnych frameworków). Strona obsługuje sesję, która trwa 10 minut. Po rejestracji jako  kupujący lub zalogowaniu się jako guest z hasłem 'Haslo111' można:
        <ul class='margines-lewy' style=' overflow:hidden;'> bez overflow:hidden bullets bedą wylazic w lewo
          <li class=''>dodawać i usuwać przedmioty z koszyka</li>
          <li class=''>składać zamówienie(sklep wysyła zamówienie emailem do kupującego jak również do sprzedającego</li>
          <li class=''>wyświetlać listę swoich zamówień</li>
          <li class=''>wyświetlać szczegóły poszczególnych zamówień</li>
          <li class=''>filtrować przedmioty według ceny i wyszukiwać według słowa kluczowego</li>
        </ul>
        Po zalogowaniu się jako 'admin' z hasłem 'Haslo111', można również usuwać i dodawać przedmioty ze zdjęciem i opisem do bazy danych klikając na Admin->Dodaj Przedmiot. Admin może również zmieniać nazwę, opis i cenę przedmiotu oraz usuwać zamówienia z bazy.
      </h5> -->

    </div> <!-- koniec diva nr 46-->

    <div class='row '>

";
//if( isset($_POST['pokaz_id']) )print "pokaz_id przed kafelkami:$_POST[pokaz_id]";
//  poczatek kafelkow Kafealki kafelki kafelki
if( (($tab)) && (empty($_POST['pokaz_id'])) ) foreach($tab as $klucz=>$wartosc){
  print "
      <div class='col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 border border-success pt-1' style='height:250px;cursor:pointer;'>
        <div style='width:100%;height:90%;display:inline-block;'>
          <img src='$wartosc[zdjecie]' onclick='pokaz_przedmiot($wartosc[id],$paymentSuccessful)' style='width:222px;height:222px;'>";
          if(isset($_SESSION['login']) && ($_SESSION[login]=='admin') )
            print "
          <img src='usun.jpeg' class='lapka' onclick='usun($wartosc[id])'  style='position:absolute;left:197px;top:0px;width:40px; height:40px;'>
                  ";
            print "
        </div>
        <div  style='width:100%;height:10%;display:inline-block;'>
          $wartosc[nazwa] Cena:$wartosc[cena]zł
        </div>
      </div>";

                   }  // koniec kafelkow, koniec kafelkow




print "
    </div>  <!-- koniec row-->
  </div>   <!-- koniec container-->
             ";
//--------------------------------------------Pokazywanie przedmiotu----------------------------------------------
if( (isset($_POST['pokaz_id'])) && !(empty($_POST['pokaz_id'])) ){
  //empty lepsza od isset bo jak jest wyslane z tej samej formy to sa wszystkie pola nawet puste inicjalizowace
  //print "isset pokazid: ";
  //print $_POST['pokaz_id'];
  if( (isset($_SESSION['login'])) && ($_SESSION['login']=='admin') ){ //ustawienie mozliwosci edycji jesli admin
    $adm=true;
    $odczyt='';
  }else{
    $odczyt='readonly';
    $adm=false;
  }
  print "<center><h1>Dane o przedmiocie</h1></center>";
  $q=$db->query("select nazwa,opis,zdjecie,id,cena from przedmioty where id='$_POST[pokaz_id]'");
  if(isset($q)) $tab=$q->fetch(PDO::FETCH_ASSOC);
  print "
    <div style='float:left;width:400px;height:400px;' class='border border-success ml-1' >
      <img src='$tab[zdjecie]' style='width:400px;height:398px;'/>
    </div>
    <div class='border border-success' style='float:left;width:400px;height:400px'>
      <h2 style='height:10%;'>
        <center>
          <input style='width:100%;text-align:center' $odczyt type='text' name='zmien_nazwa' value='$tab[nazwa]'  form='zmien_przedmiot'>
        </center>
      </h2>
      <!--<input name='zmien_opis' form='zmien_przedmiot' $odczyt value='$tab[opis]'>-->
      <textarea name='zmien_opis'  form='zmien_przedmiot' style='width:100%;height:88%;resize:none;'   $odczyt> $tab[opis] </textarea>
    </div>
    <div class='border-success border' style='float:left;width:400px;height:400px;position:relative;left:0px;top:0px;'>
      <span style='text-align:center'><center><b>Cena :
      <input style='color:black;text-align:center;width:100px;' type='text' $odczyt value='$tab[cena]' name='zmien_cena' form='zmien_przedmiot'  style='width:20%;opacity:0.8;text-align:center'>
          zł</b></center></span><br><br><br>
  ";
  if($adm)

   print "
   <form id='zmien_przedmiot'  action='#' method='post'>
      <input name='zmieniony' type='hidden' value='true'>
      <input name='cena_min' value='0' type='hidden'>
      <input type='hidden' name='zmieniony_id' value='$_POST[pokaz_id]'><br><br><br><br><br><br><br>
      <center><button class='btn btn-success btn-lg' type='submit'>Zapisz zmiany</button></center>


          </form>
          ";
  else{
    print "

      <center>
        <form action='#' method='post'>
          Liczba sztuk
          <input style='width:60px;' type='number' name='liczba_do_koszyka' placeholder='liczba sztuk' value='1'  required><br>
          <input name='cena_min' type='hidden' value='0'>
          <input name='id_do_koszyka' type='hidden' value='$_POST[pokaz_id]'>
          <input name='nazwa_do_koszyka' type='hidden' value='$tab[nazwa]'>
          <input name='cena_do_koszyka' type='hidden' value='$tab[cena]' ><br>
          <button type='submit' class='btn btn-primary btn-lg'>Dodaj do koszyka</button><br><br>
        </form>

        <form action='#' method='post' id='formularz_pokaz_koszyk' >
          <input name='pokaz_koszyk' type='hidden' value='tak'>
          <input name='pokaz_cena' id='pokaz_cena' type='hidden' value='$tab[cena]' >
          <input name='usun_z_koszyka'  type='text' value='0' hidden>
          <input name='cena_min' type='hidden' value='0'>
          <button class='btn btn-info btn-lg' type='submit'>Pokaż koszyk</button>
        </form>
      </center>
    ";
  }
  print "
    </div>



  ";
}
//------------------------------------Koniec pokazania danych o przedmiocie ------------------------------------------------

}catch(PDOException $e){ //-----------Koniec duzego try----------------------------------------
  print "Błąd bazy danych:".$e->getMessage();
}
} //koniec duzego if POST
////----------------------------------------------------------


if( ($_SESSION['login']  && $_SESSION['login']!='admin') || (!$_SESSION['login']))     print "
    <!-- ten formularz poza duzym post bo sie czesto nie wyswietli w ogole jak .np. bedzie post-->
    <form action='#' method='post' id='drugi_formularz_pokaz_koszyk' hidden >
          <input name='pokaz_koszyk' type='hidden' value='tak'>
          <input name='pokaz_cena' id='druga_pokaz_cena' type='hidden' value='999' >
          <input name='usun_z_koszyka'  type='hidden' value='0' >
          <input name='cena_min' type='hidden' value='0'>
          <button class='btn btn-secondary btn-lg' type='submit'>Pokaż koszyk</button>
        </form>
";
?>
    <div style='clear:both;'><center><h1><a href='./koncowy.php'>Powrót do strony głównej</a></h1></center></div>
  <div id='footer'>
    <footer>
      <div class='oplyw1'>
        <div class="pos-f-t">
          <div class="collapse" id="navbarToggleExternalContent">
            <div class="bg-dark p-4">
              <h5 class="text-white h4">W przygotowaniu<br>
                <a hidden href='http://www.wp.pl'>wirtualna polska</a><br>
                <a hidden href='http://www.onet.pl'>onet</a>
              </h5>
              <span class="text-muted">W przygotowaniu</span>
            </div>
          </div>
          <nav class="navbar navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </nav>
        </div>
      </div>
    </footer>
  </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<?php
print<<<HTML
    <form action='$_SERVER[PHP_SELF]' method='post' id='form_kategorie'>
        <input type='hidden' name='ukryty' id='ukryty'>

    </form>
HTML;
    ?>
<?php
print "
  <form action='#' method='POST' id='forma' hidden >
    <input type='text' name='pokaz_id' id='pokaz_id'>
    <input type='text' name='usun_id'  id='usun_id'>
    <input type='hidden' name='cena_min' id='cena_minimalna' value='0'>
    <input type='hidden' name='temporacyjny'>
    <button type='submit'>przeslij</button>
  </form>
  <form action='#' method='post' id='po_usunieciu'>  <!--jakby tamta nie dzialala to tej formy uzyc i zmienic na id z ind-->
    <input type='hidden' name='usun_id'  id='usun_id'>
    <input type='hidden' name='cena_min' ind='cena_minimalna' value='0'>
  </form>
  <form action='#' id='formaZlozZamowienie' method='post' hidden>
    <input type='text' name='zamowienie' value='true'>
    <!--<button type='submit'>zamowionoko</button>-->
  </form>
  <form id='moje_zamowienia' action='#' method='post'>
    <input type='hidden' name='form_pokaz_zamowienia' value=true>
    <input type='hidden' id='zamowienie_do_usuniecia'      name='zamowienie_do_usuniecia' value='0'>
  </form>
  <form action='#' id='pojedynczeZamowienie' method='post' hidden>
    <input type='text' id='numer_zamowienia' name='numer_zamowienia' value='numer_zamowienia'>
  </form>
  <!--forma:form:usun_z_koszyka<br>-->
  <form action='#' method='post' id='form_usun_z_koszyka' hidden> name:pokaz_koszyk<input name='pokaz_koszyk' type='text' value='true'><br>
    name:pokaz_cena
    <input name='pokaz_cena' type='text' value='999.999' ><br>
    name:usun_z_koszyka<input name='usun_z_koszyka' id='usun_z_koszyka' type='text' value='0'><br>
    <button class='btn btn-secondary btn-lg' type='submit'>pomocniczy do usuniecia z koszyka</button>
  </form><br>
";
?>




<script>
  function funkcja(numer){
    // document.getElementById('ukryty').value='jffjfj';
    //document.getElementById('form_kategorie').submit();
    var kategoria;
    if(numer==1)document.getElementById('ukryty').value='Kwiaty';
    else if(numer==2)document.getElementById('ukryty').value='Drzewa Iglaste';      // else if nie moze byc razem w java scripcie czyli nie moze byc elseif
    //document.getElementById('form_kategorie').innerHTML+='ewfwefvvvvvvvvvvvvvvvvvvwefewf';//jakos resetuje powyzszego if
    else if(numer==3)document.getElementById('ukryty').value='Drzewa liściaste';
    else if(numer==4)document.getElementById('ukryty').value='Róże';
    else if(numer==5)document.getElementById('ukryty').value='Preparaty';
    document.getElementById('form_kategorie').submit();
  }
  function usun(id){
    // document.body.innerHTML+=id;
    document.getElementById('usun_id').value=id;
    //document.getElementById('cena_minimalna').value='0';
    document.getElementById('forma').submit();
    // document.body.innerHTML+=id;
  }
  function replaceWindow(){
    window.location = "./koncowy.php";
  }
  function pokaz_przedmiot(id,paymentSuccessful){
      if(paymentSuccessful == true){
      replaceWindow();
    }
    else { //inaczej sie nie da bo wraca i pokaze przedmiot ale wtedy juz nie dziala replaceWindow()
    var krowa=new Audio('sheep.wav');
    if(id===59){
        krowa.play();
    alert('uwaga owce');}
    document.getElementById('pokaz_id').value=id;
    //document.body.innerHTML+=id;
    document.getElementById('forma').submit();
    document.getElementById('dupa').innerHTML='dupa biskupa';
  }
  }
  function logowanie(){
    location.replace('./logowanie.php');
  }
  function form_moje_zamowienia(id){
    document.getElementById('zamowienie_do_usuniecia').value=id;
    document.getElementById('moje_zamowienia').submit();
  }
  function pojedynczeZamowienie(numer){
    document.getElementById('numer_zamowienia').value=numer;
    document.getElementById('pojedynczeZamowienie').submit();
    //alert("javascript pojedynczeZamowienie");
  }

  function usun_z_koszyka(usun_z_koszyka){  //usun_z_koszyka to numer porzadkowy tablicy $_SESSION['koszyk']
    //alert('funkcja usun_z_koszyka,indeks do usuniecia to  '+usun_z_koszyka);
    document.getElementById('usun_z_koszyka').value=usun_z_koszyka;
    //document.getElementById('gutu').value=usun_z_koszyka;
    document.getElementById('form_usun_z_koszyka').submit();
  }
  function formularz_pokaz_koszyk(){

     document.getElementById('formularz_pokaz_koszyk').submit();
    }
  function drugi_formularz_pokaz_koszyk(paymentSuccessful){
    if(paymentSuccessful == true){
    replaceWindow();
  }
  else {
      //document.getElementById('pokaz_cena').value='999.9';
      //alert(44);

      document.getElementById('drugi_formularz_pokaz_koszyk').submit();
      //alert('a_koszyk');
    }
  }
</script>




<!--forma tutu, input gutu -->
<!--<form id='tutu'><input id='gutu' hidden></form>-->
</section>
</main>

<?php
  function pokazKoszyk($cena,$usun_z_koszyka){

    if($usun_z_koszyka)print "<center><h4>Przedmioty $usun_z_koszyka zostały usunięte z koszyka</h4><center> <br>";
    //print "funkcja pokazKoszyk sessionlogin:".$_SESSION['login']."<br>";
    //print "funkcja pokazKoszyk cena: $cena ".$cena." po cenie<br>";
    $tablica=array();
    $ilosc=0;
    foreach($_SESSION['koszyk'] as $id=>$nazwa1){
      foreach($nazwa1 as $klucz_nazwa=>$nazwa){
        if($klucz_nazwa==$usun_z_koszyka){
          foreach($nazwa as $key=>$value){
            if($key=='liczba')$ilosc=$value;
            if($key=='cena'){
              $_SESSION['total']-=$ilosc*$value;
              unset($_SESSION['koszyk'][$id]);//tak mozna mimo ze jeszt czterowymiarowa!!!!!!!!!!!!!
            }
          }
        }else{
          foreach($nazwa as $key=>$value){   //$key to 'liczba' lub 'cena'   a   $value to $liczba lub $cena
          //print "<br> id:$id nazwa1:$nazwa1 klucz_nazwa:$klucz_nazwa nazwa:$nazwa key:$key value:$value<br>";
          $tablica[$klucz_nazwa][$key]=$value;
          //print $tablica[$klucz_nazwa][$key].",";
          }
        }
      }
    }
    ksort($tablica);
    print "<center><h2>Zawartość koszyka</h2></center>";
    print "<center><table class='table-striped'>";
    foreach($tablica as $nazwa=>$klucz){
      foreach($klucz as $key=>$wartosc){
        if($key=='liczba')print "<tr><td>$nazwa</td><td>: sztuk x {$tablica[$nazwa]['liczba']}</td>";
        if($key=='cena'){  print "<td> Cena : {$tablica[$nazwa][$key]}</td><td> zł</td><td><button class='btn btn-danger btn-sm' ";
      print<<<HTML
        onclick="usun_z_koszyka('$nazwa')"
HTML;
      print ">Usuń</button></td></tr>";
        }
      //<!-- trzeba az tak rozbic i cudaczyc zeby dac double quotes bo inaczej sie string nie przesle z php do javascriptu-->
      //print "tablica[nazwa]:$tablica[$nazwa] tablica:".$tablica."<br>nazwa:".$nazwa."<br>klucz:".$klucz."<br>key".$key."<br>wartosc".$wartosc."<br>";
      }
    }
    if($_SESSION['total']<0.01)$_SESSION['total']=0; //bo wychodzą jakieś 5 do potęgi minus 15 jesli usuniemy po kolei wszystkie przedmioty z koszyka
    print "<tr><td></td><td></td><td colspan='2' class='border-bottom border-success' style='border-width:3px !important;'></td></tr>
          <tr><td></td><td></td><td> Suma : $_SESSION[total]  </td><td>zł</td></tr>";
    if(isset($_SESSION['total']) && $_SESSION['total'])$aktywny="";
    else $aktywny='disabled'; //dezaktywuje przycisk zlow zamowienie gdy koszyk jest pusty
    if($_SESSION['login'] && $_SESSION['login']!='admin'){
      print "<tr><td colspan='4'><button form='formaZlozZamowienie'  type='submit' class='btn btn-success btn-lg w-100' $aktywny>Złóż zamówienie</button></td></tr>
      ";
    }
    else if(!$_SESSION['login']){
      print "<tr><td colspan='4'><button onclick='logowanie();' class='btn btn-success btn-lg w-100'>Logowanie</button></td></tr>";
    }
    print "
      </table></center>
    ";

  }// koniec funkcji pokazKoszyk
//---------------------------------------------------------------------------------------------------------------------------
  function zloz_zamowienie(){
  //$id_zamowienia;
  // cos nie można wyświetlac obrazow <img src=obraz.jpg> bo sie zawiesza funkcja, ale po podaniu pełnej ściezki zamiast wzglednej to sie wyswietla mala ikonka zamiast obrazu!!! ale w innej funkcji dziala normalnie w sciezce wzglednej
  try{
    $db=new PDO('mysql:host=localhost;dbname=db','uzytkownik','pass');
    $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    $db->query("SET CHARSET utf8");                             //bez tego nie będzie polskich czcionek przesłanych z php
    $db->query("SET NAMES 'utf8' COLLATE 'utf8_polish_ci'");    ////bez tego nie będzie polskich czcionek przesłanych z php
    $db->exec("insert into zamowienia values(0,(select id from klienci where login='$_SESSION[login]'),now(),$_SESSION[total])");
    foreach($_SESSION['koszyk'] as $id=>$nazwa1){
      foreach($nazwa1 as $klucz_nazwa=>$nazwa){
        foreach($nazwa as $key=>$value){   //$key to 'liczba' lub 'cena'   a   $value to $liczba lub $cena
          if($key=='liczba'){
            $db->exec("insert into szczegoly_zamowienia values((select id from przedmioty where nazwa='$klucz_nazwa'),(select last_insert_id()),$value)");
          };
          //$db->exec("insert into szczegoly_zamowienia values ("");
          //print $klucz_nazwa.":".$key.":".$value."<br>";
          //print $tablica[$klucz_nazwa][$key].",";
        }
      }
    }// koniec glownego foreach
  //.............................................Wyslanie zamowienia emailem................................................
  $q=$db->query("select last_insert_id()");
  $gab=$q->fetch(PDO::FETCH_ASSOC);
  $id_zamowienia=$gab['last_insert_id()'];
  $q=$db->query("select imie,nazwisko,email,telefon,klienci.id,zamowienia.data,zamowienia.total from klienci inner join zamowienia on klienci.id=zamowienia.id_klienta where zamowienia.id=$id_zamowienia");
  $tab=$q->fetch(PDO::FETCH_ASSOC);
  $total=$tab['total'];
  $email_klienta=$tab['email'];
  $imie_nazwisko="$tab[imie] $tab[nazwisko]";
  $zamowienie_do_wyslania.="
    <center>
      <div>
        <table  class='table-striped border border-warning table-bordered' style='border:4px solid !important;border-color:orange !important;'>
          <tr><td><b>Zamowienie nr</b></td><td><b>$id_zamowienia</b></td></tr>
          <tr><td>Numer klienta</td><td>$tab[id]</td></tr>
          <tr><td>Imie</td><td>$tab[imie]</td></tr>
          <tr><td>Nazwisko</td><td>$tab[nazwisko]</td></tr>
          <tr><td>Email</td><td>$tab[email]</td></tr>
          <tr><td>Telefon</td><td>$tab[telefon]</td></tr>
          <tr><td>data</td><td>$tab[data]</td></tr>
          <tr><td>Suma</td><td>$tab[total] zł</td></tr>
        </table></div></center><br><br><br>
  ";
  $q=$db->query("select id,nazwa,cena,szczegoly_zamowienia.ilosc from przedmioty inner join szczegoly_zamowienia on id=szczegoly_zamowienia.id_przedmiotu where id_zamowienia=$id_zamowienia");
  $tab=$q->fetchall(PDO::FETCH_ASSOC);
  $zamowienie_do_wyslania.="
    <center>
      <div class='border border-primary d-inline-block border-lg'>
        <table class='table-hover table-bordered border border-warning ' style='border-style:solid !important;border-width:2px !important;border-color:blue !important;'>
        <tr><th>id </th><th>Nazwa</th><th>Cena</th><th>Ilość</th></tr>
  ";
  foreach($tab as $key=>$value){
    $zamowienie_do_wyslania.="
      <tr><td>$value[id]</td><td>$value[nazwa]</td><td>$value[cena] zł</td><td>$value[ilosc]</td></tr>
    ";
  }
  $zamowienie_do_wyslania.="
          <tr><th colspan=2>Suma</th><th colspan=2>$total zł</th>
        </table>
      </div>
    </center>
  ";
//...................PHPMailer................................
  $poczta=new PHPMailer(true);
  try{
    //$poczta->SMTPDebug=2;
    $poczta->isSMTP();
    $poczta->Host='smtp.gmail.com';
    $poczta->SMTPAuth=true;
    $poczta->Username='user';
    $poczta->Password='pass';
    $poczta->SMTPSecure='tls';
    $poczta->CharSet='utf-8';
    $poczta->Port=587;
    $poczta->setFrom("email@email.com","Sklep Wirtualny");  //tutaj nawet jak sie wpisze inny email to i tak bedzie potem
    $poczta->AddAddress("$email_klienta");
    $poczta->addReplyTo("email@email.com");
    $poczta->isHTML(true);
    $poczta->Subject="Złożyłeś zamówienie w naszym sklepie";
    $poczta->Body=$zamowienie_do_wyslania;
    //wysłanie email z zamówieniem do klienta
    //$poczta->send();  //zakomentowałem bo trzeba długo czekać a i tak nikt tego nie sprawdza
    //// wysłanie email z zamówieniem do sklepu
    $poczta->ClearAddresses();  //kasuje adresy odbiorcow , bo inczej wysle dodatkowo emaila do sklepu, który był wysłany do klienta, ale do klienta wyśle też
    $poczta->AddAddress('email@email.com');
    $poczta->Subject="Klient $imie_nazwisko złożył zamówienie nr $id_zamowienia";
    //$poczta->send(); //zakomentowałem bo trzeba długo czekać a i tak nikt tego nie sprawdza
    $totalToSend = $_SESSION['total'];
    print "<h1><center>Zamówienie zostało złożone pomyślnie <img src='ok.jpg' style='width:50px'/></center></h1>";
    print "<h1>
             <center>
             <h1 class='text-success'>Zapłać:</h1>
               <form action='../symbank/public/store_login' method='POST'>
                 <input type='text' hidden name='total' value='$total' />
                 <input type='text' hidden name='storeName' value='sklepOgrodniczy' />
                 <input type='text' hidden name='orderNumber' value='$id_zamowienia' />
                 <input type='text' hidden name='urlBack' value='/projekt/koncowy.php' />
                 <input type='text' hidden name='storeAccountNumber' value='888888888888' />
                 <button class='btn btn-success p-1' type='submit'>symBank</button> <button class='btn btn-secondary p-1' disabled>coolBank</button> <button class='btn btn-secondary p-1' disabled>oldBank</button>
               </form>
             </center>
           </h1>";
    unset($_SESSION['koszyk']);
    unset($_SESSION['total']);
  }catch(Exception $ex){
    print 'Wystąpił błąd podczas wysyłania zamówienia'.$poczta->ErrorInfo;
  }
//.................................Koniec PHPMailer.......................................
//.................................Koniec wyslania zamowienia emailem.........................
  }catch(PDOException $e){          //koniec try PDO
    print "Błąd Mysql:".$e->getMessage();
  }
  }//koniec funkcji zlow zamowienie
//-----------------------------------------------------------------------------------------------------------------------------------------
function form_pokaz_zamowienia($zamowienie_do_usuniecia){
  $db=new PDO('mysql:host=localhost;dbname=db','uzytkownik','pass');
  $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $db->query("SET CHARSET utf8");                             //bez tego nie będzie polskich czcionek przesłanych z php
  $db->query("SET NAMES 'utf8' COLLATE 'utf8_polish_ci'");////bez tego nie będzie polskich czcionek przesłanych z php
  //print"<center><h1>Zamówienia</h1></center>";
  if($_SESSION['login']=='admin'){
    $adm=true;
  }else $adm=false;
  if($adm){
    if($zamowienie_do_usuniecia>0){ //usuniecie zamowienia
      $db->exec("delete from szczegoly_zamowienia where id_zamowienia='$zamowienie_do_usuniecia';");
      $db->exec("delete from zamowienia where id='$zamowienie_do_usuniecia';");
      print"<center><h2>Zamówienie nr $zamowienie_do_usuniecia zostało usunięte</h2></center>";
    }
  $q=$db->query("select id,id_klienta,data,total from zamowienia;");
  $tab=$q->fetchall(PDO::FETCH_ASSOC);
  print "
    <center>
      <table style='text-align:left' class='lapka table-hover table-striped border border-warning table-bordered'>
        <tr style='text-align:left'><th>Numer zamówienia</th><th>Numer klienta</th><th>Data</th><th>Suma</th></tr>
  ";
  foreach($tab as $klucz=>$wartosc){
    print "
      <tr id='$wartosc[id_klienta]'>
        <td onclick='pojedynczeZamowienie($wartosc[id])'>$wartosc[id]</td>
        <td onclick='pojedynczeZamowienie($wartosc[id])'>$wartosc[id_klienta]</td>
        <td onclick='pojedynczeZamowienie($wartosc[id])'>$wartosc[data]</td>
        <td onclick='pojedynczeZamowienie($wartosc[id])'>$wartosc[total]</td>
        <td><button type='button' class=' btn btn-warning czerwony'><span onclick='form_moje_zamowienia($wartosc[id])' class='czerwony'>Usuń</span></button></td></tr>
    ";
      }
    print "
      </table>
        </center>
    ";
    //print "<span id='spanonclick' onclick='alertujemy()'>spanonlick</span>";
  }else{ //pokazanie zamowien pojedynczego zalogowanego klienta
    print"<center><h1>Zamówienia</h1></center>";
    $q=$db->query("select id,id_klienta,data,total from zamowienia where id_klienta=(select id from klienci where login='$_SESSION[login]');");
    $tab=$q->fetchall(PDO::FETCH_ASSOC);
    print "
      <center><table style='text-align:left' class='lapka table-hover table-striped border border-warning table-bordered'>
      <tr style='text-align:center'><th>Numer zamówienia</th><th>Numer klienta</th><th>Data</th><th>Suma</th></tr>
    ";
    foreach($tab as $klucz=>$wartosc){
      print "<tr id='$wartosc[id_klienta]' onclick='pojedynczeZamowienie($wartosc[id])'><td>$wartosc[id]</td><td>$wartosc[id_klienta]</td><td>$wartosc[data]</td><td>$wartosc[total]</td></tr>
      ";
    }
    print "
      </table></center>
    ";

  }// koniec pokazania zamonien pojedynczego zalogowanego

} //koniec funkcji form_pokaz_zamowienia
//----------------------------------------------------------------------------------------------------------------
function pojedynczeZamowienie($id_zamowienia){
  //print "<span >pokazanie pojedynczego zamowienia nr zamowienia $id_zamowienia<span>";
  $db=new PDO('mysql:host=localhost;dbname=db','uzytkownik','pass');
  $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $db->query("SET CHARSET utf8");                             //bez tego nie będzie polskich czcionek przesłanych z php
  $db->query("SET NAMES 'utf8' COLLATE 'utf8_polish_ci'");    ////bez tego nie będzie polskich czcionek przesłanych z php
  $q=$db->query("select imie,nazwisko,email,telefon,klienci.id,zamowienia.data,zamowienia.total from klienci inner join zamowienia on klienci.id=zamowienia.id_klienta where zamowienia.id=$id_zamowienia");
  $tab=$q->fetch(PDO::FETCH_ASSOC);
  $total=$tab['total'];
  print "
    <center>
      <div>
        <table  class='table-striped border border-warning table-bordered' style='border:4px solid !important;border-color:orange !important;'>
          <tr><td><b>Zamowienie nr</b></td><td><b>$id_zamowienia</b></td></tr>
          <tr><td>Numer klienta</td><td>$tab[id]</td></tr>
          <tr><td>Imie</td><td>$tab[imie]</td></tr>
          <tr><td>Nazwisko</td><td>$tab[nazwisko]</td></tr>
          <tr><td>Email</td><td>$tab[email]</td></tr>
          <tr><td>Telefon</td><td>$tab[telefon]</td></tr>
          <tr><td>data</td><td>$tab[data]</td></tr>
          <tr><td>Suma</td><td>$tab[total] zł</td></tr>
        </table></div></center><br><br><br>
  ";
  $q=$db->query("select id,nazwa,cena,szczegoly_zamowienia.ilosc from przedmioty inner join szczegoly_zamowienia on id=szczegoly_zamowienia.id_przedmiotu where id_zamowienia=$id_zamowienia");
  $tab=$q->fetchall(PDO::FETCH_ASSOC);
  print "
    <center>
      <div class='border border-primary d-inline-block border-lg'>
        <table class='table-hover table-bordered border border-warning ' style='border-style:solid !important;border-width:2px !important;border-color:blue !important;'>
          <tr><th>id </th><th>Nazwa</th><th>Cena</th><th>Ilość</th></tr>
  ";
  foreach($tab as $key=>$value){
    print "
      <tr><td>$value[id]</td><td>$value[nazwa]</td><td>$value[cena] zł</td><td>$value[ilosc]</td></tr>
    ";
  }
  print "
    <tr><th colspan=2>Suma</th><th colspan=2>$total zł</th>
      </table></div></center>";
}// koniec funkcji pojedynczeZamowienie
//------------------------------------------------------------------------------------------
?>

<!-- <table style='background:yellow;'><tr><td>aaa</td><td>bbb</td><td>ccc</td></tr></table>-->
</body>
</html>


<?php
function ikona($benek){
print "eeee ikona <img src='fail.jpg' width='50px'>";
}
//---------------------------------------------------------------fukcja dodajdokoszyka-----------------------------------
 function DodajDoKoszyka($id,$nazwa,$liczba,$cena){   //cos z ta funkcja jest bo w funkcji ikona mozna bez problemu wyswietlic ikone fail

      //print "<img src='fail.jpg' width='50px' alt='brak obrazu'>";
    //print "DodajDoKoszyka".$id.$nazwa.$cena."<br>";
    //print "DodajDoKoszyka post nazwadokoszyka $_POST[nazwa_do_koszyka] <br>";
    if($liczba<0)$liczba=-$liczba;
    $_SESSION['koszyk'][$id][$nazwa]['liczba']+=$liczba;
    $_SESSION['koszyk'][$id][$nazwa]['cena']=$cena;
    $_SESSION['total']+=($liczba*$cena);
    //print "ddDodajDoKoszyka:".$_SESSION['koszyk'][$id][$nazwa]['liczba']." ".$_SESSION['koszyk'][$id][$nazwa]['cena']."<br>";
    if($liczba=='1'){
     //print "eeee ikona <img src='fail.jpg' width='50px' alt='niemahekrata'>";
      print "
        <center><h1>Przedmiot został dodany do koszyka <img src='ok.jpg'  style='width:50px'/></h1></center>
      ";}
    else if($liczba>1)print "
                        <center><h1>Przedmioty zostały dodane do koszyka <img src='ok.jpg' style='width:50px'/></h1></center>
                      ";
  } //koniec DodajDoKoszyka
//-----------------------------------------------------------------------------------------------------------------------------

//-----------------------------baza()---------------------------------------


function showMessage()
{
      print "<h1><center class='h1 text-success'>$_GET[message] <img src='ok.jpg' style='width:50px'/></center></h1>";



}

  ?>
